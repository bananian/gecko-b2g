# -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
# vim: set filetype=python:
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

EXPORTS += [
    "malloc_decls.h",
    "mozjemalloc_types.h",
    "mozmemory.h",
    "mozmemory_wrap.h",
]

LIBRARY_DEFINES["MOZ_HAS_MOZGLUE"] = True
DEFINES["MOZ_MEMORY_IMPL"] = True

if CONFIG["MOZ_REPLACE_MALLOC"]:
    EXPORTS += [
        "replace_malloc.h",
        "replace_malloc_bridge.h",
    ]

if CONFIG["MOZ_PHC"]:
    DEFINES["MOZ_PHC"] = True

if CONFIG["MOZ_MEMORY"]:
    UNIFIED_SOURCES += [
        "mozjemalloc.cpp",
        "mozmemory_wrap.cpp",
    ]
else:
    UNIFIED_SOURCES += [
        "fallback.cpp",
    ]

if CONFIG["OS_TARGET"] == "Darwin" and (
    CONFIG["MOZ_REPLACE_MALLOC"] or CONFIG["MOZ_MEMORY"]
):
    SOURCES += [
        "Mutex.cpp",
        "zone.c",
    ]

Library("memory")

if CONFIG["OS_TARGET"] == "Android" and CONFIG["CC_TYPE"] == "clang":
    CXXFLAGS += [
        "-Wno-tautological-pointer-compare",
    ]

if CONFIG["MOZ_BUILD_APP"] != "memory":
    FINAL_LIBRARY = "mozglue"

if CONFIG["MOZ_REPLACE_MALLOC_STATIC"]:
    DEFINES["MOZ_REPLACE_MALLOC_STATIC"] = True

DisableStlWrapping()

if CONFIG["CC_TYPE"] == "clang-cl":
    AllowCompilerWarnings()  # workaround for bug 1090497

# Experimental: stall and retry on OOM (bug 1716727).
#
# This _will_ induce performance regressions in some (hopefully rare) contexts:
# we don't know whether any given allocation is supposed to be fallible, so we
# have to stall on any OOMed allocation.
#
# This is remediable, in theory. For now, though, we simply restrict it to
# Nightly, to give us enough data to determine whether this approach is worth
# the trouble of pursuing further.
#
# This experiment should probably not be active beyond 2022-09-01, and may be
# terminated early if the performance regression turns out to be worse than
# anticipated.
if CONFIG["NIGHTLY_BUILD"] and CONFIG["OS_TARGET"] == "WINNT":
    DEFINES["MOZ_STALL_ON_OOM"] = True

REQUIRES_UNIFIED_BUILD = True
