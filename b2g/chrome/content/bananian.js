/* -*- indent-tabs-mode: nil; js-indent-level: 2 -*- /
/* vim: set shiftwidth=2 tabstop=2 autoindent cindent expandtab: */
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

window.addEventListener("systemappframeprepended", () => {
  let isApp = true, url = "http://b2g.localhost:8081/manifest.webmanifest";
  let manifestParam = new URL(window.location).searchParams.get("manifest");
  if(manifestParam){
    url = manifestParam;
  }
  else if(window.arguments && 'string' === typeof window.arguments[1]){
    url = window.arguments[1];
    isApp = window.arguments[2];
  }
  else {
    try {
      const args = Cc[
        "@mozilla.org/commandlinehandler/general-startup;1?type=b2gcmds"
      ].getService(Ci.nsISupports).wrappedJSObject.cmdLine;
      url = args.handleFlagWithParam("manifest", false) || url;
    } catch (e) {
      console.error(e);
    }
  }

  const systemAppFrame = document.getElementById("systemapp");
  const handler = () => {
    systemAppFrame.classList.add("fullscreen");
    systemAppFrame.removeAttribute("style");
    console.log(`-*- Bananian: sending URL ${url} to system app. app=${isApp}`);
    if(isApp){
      systemAppFrame.contentWindow.postMessage({ manifestURL: url }, "*");
    }
    else {
      systemAppFrame.contentWindow.postMessage({ url }, "*");
    }
  };
  systemAppFrame.style.width = "240px";
  systemAppFrame.style.height = "295px";
  systemAppFrame.contentWindow.addEventListener("DOMContentLoaded", handler);
  systemAppFrame.contentWindow.addEventListener("windowclose", () => {
    window.close();
  });
});
